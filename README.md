
## Sujets  de la consolidation

Ce dépot contient des sujets pour les oraux de consolidation

```bash
git clone https://gitlab.com/adobet/orauxconsolidations_sujets
```

## Plannings de la consolidation

Les résultats des QCM du vendredi et les plannings sont disponibles sur un autre dépot :

`https://gitlab.com/adobet/OrauxConsolidation`
