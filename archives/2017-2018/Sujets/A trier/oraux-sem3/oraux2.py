# représentation mémoire avec listes et appels de fonctions
l1 = [1,[2, 3],4]
l2 = [6,7,[]]
l3 = [l1[1],l2]
#représentation mémoire de l1, l2, l3
print(l1) # 
print(l2) #
print(l3) # [[2,3],[6,7,[]]]
l3[1][1] = l2[2] 
l2[2] = l1[1]
##représentation mémoire de l1, l2, l3
print(l1) # [1,[2,3],4]
print(l2) # [6,[],[2,3]]
print(l3) # [[2,3],[6,[],[2,3]]]

#slicing
print(l1[1:]) #-> [[2,3],4]
print(l2[1:2]) #-> [[]]
print(l3[-2:])#-> [[2,3],[6,[],[2,3]]]
print("fin 2")

# appels de fonctions imbriquées
def f4(liste) :
    res = []
    for i in range(len(liste)) :
        if liste[i] %3 == 0 :
            res.append(liste[i] -1)
        else :
            res.append(liste[i])
    return res

def f5(liste) :
    res = []
    for i in range(len(liste)) :
        if i % 2 == 0 :
            res.append(liste[i])
        else :
            res.append(liste[i]+1)
    return res

def f6(liste) :
    res = 0
    for elem in liste :
        if elem %2 == 0:
            res += elem
        else :
            res -= elem
    return res


l = [1,2,6,4,9,12,11]
#f4(l) -> [1,2,5,4,8,11,11]
#f5(f4(l)) -> [1,3,5,5,8,12,11]
print(f6(f5(f4(l)))) #-5


# produits avec (nom, prix, qte)
# produit ayant la plus petite quantité avec son noms sa quantité et son prix
#produit ayant le plus petit prix (mutualiser) juste son nom
# somme totale
#liste avec nom et prix total par produit

def plusPetiteQuantite(liste) :
    if len(liste) == 0 :
        res = None
    else :
        res = liste[0]
        for elem in liste[1 : ] :
            if elem[1] < res [1] :
                res = elem
    return res

def plusPetitPrix(liste) : 
    if len(liste) == 0 :
        res = None
    else :
        res = liste[0]
        for elem in liste[1 : ] :
            if elem[2] < res [2] :
                res = elem
    return res[0]

def plusPetitPrixIndice(liste) : 
    if len(liste) == 0 :
        res = None
    else :
        res = 0
        for i in range(1, len(liste)):
            if liste[i][2] < liste[res][2] :
                res = i
    return res

def sommeTotale(liste) :
    som = 0
    for (_, qte, p) in liste :
        som += p * qte
    return som


def produitsPrixTotal(liste) :
    res = []
    for (nom, qte, p) in liste :
        res.append((nom, qte * p))
    return res

def supprimePlusPetit(liste, i) :
    res = []
    for j in range(len(liste)) :
        if j != i :
            res.append(liste[j])
    return res

#selon le prix ordre croissant
def tri(liste) :
    res = []
    for i in range(len(liste)) :
        ind = plusPetitPrixIndice(liste)
        v = liste[ind]
        liste = supprimePlusPetit(liste,ind)
        res.append(v)
    return res   
    
l = [('stylo',3,2.5),('crayon',5,0.75),('gomme', 2,0.5),('feuilles', 1, 8.5),('règle', 4,2.6),('taille-crayon', 3, 1.8),('effaceur',10, 0.35)]

print(plusPetiteQuantite(l)) #('feuilles', 1, 8.5)
print(plusPetitPrix(l)) #effaceur
print(plusPetitPrixIndice(l)) #6
print(sommeTotale(l))# 40.05 
print(produitsPrixTotal(l)) # [('stylo', 7.5), ('crayon', 3.75), ('gomme', 1.0), ('feuilles', 8.5), ('règle', 10.4), ('taille-crayon', 5.4), ('effaceur', 3.5)]
print(tri(l)) #[('effaceur', 10, 0.35), ('gomme', 2, 0.5), ('crayon', 5, 0.75), ('taille-crayon', 3, 1.8), ('stylo', 3, 2.5), ('règle', 4, 2.6), ('feuilles', 1, 8.5)]

