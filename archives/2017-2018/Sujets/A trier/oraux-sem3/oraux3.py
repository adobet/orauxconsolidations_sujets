# représentation mémoire avec listes et appels de fonctions
l1 = [[1,2],3,[4,5]]
l2 = [6,7,8]
l3 = [l1[0]+l1[2],l2]
#représentation mémoire de l1, l2, l3
print(l1) # [[1,2],3,[4,5]]
print(l2) # [6,7,8]
print(l3) # [[1,2,4,5],[6,7,8]]
l3[0][0] = l1[2]
l1[0][1] = l3[0][0]
##représentation mémoire de l1, l2, l3
print(l1) # [[1, [4,5]],3,[4,5]]
print(l2) # [6,7,8]
print(l3) # [[[4,5],2,4,5],[6,7,8]

#slicing
print(l1[1:]) #-> [3,[4,5]]
print(l3[1:2]) #-> [[6,7,8]]
print(l3[0][2:])#-> [4,5]

# appels de fonctions imbriquées
def f7(liste) :
    res = []
    for i in range(len(liste)) :
        if i %3 == 0 :
            res.append(liste[i] -1)
        else :
            res.append(liste[i])
    return res

def f8(liste) :
    res = []
    for i in range(len(liste)) :
        if liste[i] % 2 == 0 :
            res.append(liste[i])
    return res

def f9(liste) :
    res = []
    for i in range(len(liste)) :
        if i %2 == 0:
            res.append(liste[i]+1)
        else :
            res.append(liste[i]-1)
    return res


l = [1,2,6,4,9,12,11]
#f7(l) -> [0,2,6,3,9,12,10]
#f8(f7(l)) -> [0,2,6,12,10]
print(f9(f8(f7(l)))) # [1,1,7,11,11]

#animaux (nom, age, taille)
def ageMoyenzoo(liste) :
    som = 0
    for (_,age,_) in liste : 
        som += age
    if len(liste) == 0: 
        res = None 
    else : 
        res = som/len(liste) 
    return res

def animauxTailleMoins10(liste) : 
    res = []
    for (nom, age, taille) in liste : 
        if taille < 10 : 
            res.append((nom, age, taille))
    return res

#la plus petite taille et si plusieurs le plus grand age 
def plusPetiteTailleplusGrandAge(liste) : 
    if len(liste) == 0 : 
        res = None 
    else : 
        indtmin = 0
        for i in range(1, len(liste)) : 
            if liste[i][2] < liste[indtmin][2] : 
                indtmin = i
            else : 
                if liste[i][2] == liste[indtmin][2] : 
                    if liste [i][1] > liste[indtmin][1] :
                        indtmin = i
        res = liste[indtmin]
    return res



#trier les animaux age < 5 ans et autres
def tri(liste) : 
    res = []
    for elem in liste : 
        res.append(elem) 
    ideb = 0
    ifin = len(liste) -1
    while i < len(liste) : 
        if liste[i][1] < 5 : 
            res[ideb] = liste[i]
            ideb += 1
        else : 
            res[ifin] = liste[i]
            ifin -= 1
        i += 1
    return res