# creer une dictionnaire avec des noms de capitales et des nbres d'habitants.
capitales = {}
capitales['Pékin'] = 20693000
capitales['Tokyo'] = 13189000
capitales['Moscou'] = 11541000
capitales['Séoul'] = 10528774
capitales['Jarkarta'] = 10187595
capitales['Mexico'] = 8851080
capitales['Londres'] = 8630100
print(capitales)

# ajouter la capitale Lima avec 8481415 hab
capitales['Lima'] = 8481415

# modifier la valeur du nombres d'habitants de Moscou car le nombre d'habitants est celui de 2012 pour la valeur :15512000
capitales['Moscou'] = 15512000
print(capitales)
# afficher la listes des clefs du dictionnaire en utilisant une méthode les fournissant
print(capitales.keys())
# construire la liste des clefs par fonction

def nomsCapitales(capitales) :
    listeNoms = []
    for clef in capitales :
        listeNoms.append(clef)
    return listeNoms

print(nomsCapitales(capitales))

#definir le nom de la capitale qui a le plus d'habitants
def capitaleMax(capitales) :
    if len(capitales) == 0 :
        max = None
    else :
        habMax = 0
        for (nom, hab) in capitales.items() :
            if hab > habMax :
                max = nom
                habMax = hab
    return max

print(capitaleMax(capitales)) #-> Pékin 

#Définir un dictionnaire avec les capitales ayant plus de 12 000 000 habitants

def dicoPlus12(capitales) :
    dico = {}
    for (cap, hab) in capitales.items() :
        if hab > 12000000 :
            dico[cap] = hab
    return dico

print(dicoPlus12(capitales))

# rechercher si une capitale est dans le dictionnaire
# sinon l'ajouter vous retournerez un booléen indiquant si la
# capitale existe ou pas et le dictionnaire éventuellement
# modifié

def ajouterSiNonOk(capitales, nom, nombreHab) :
    existe = True
    if nom not in capitales.keys() :
        capitales[nom] = nombreHab
        existe = False
    return (existe, capitales)

(existe, capitales) = ajouterSiNonOk(capitales, 'Bangkok', 8249117)

if not existe :
    print(capitales)
else :
    print("capitale existante ")

# on dispose d'un dictionnaire indiquant pour une capitale dans
# quel continent elle se trouve

continent = {}
continent['Pékin'] = 'Asie'
continent['Tokyo'] = 'Asie'
continent['Moscou'] = 'Europe'
continent['Séoul'] = 'Asie'
continent['Jarkarta'] = 'Asie'
continent['Mexico'] = 'Amérique'
continent['Londres'] = 'Europe'
continent['Lima'] = 'Amérique'
continent['Bangkok'] = 'Asie'

print(continent)

#construire un dictionnaire avec le nombre de capitales du dictionnaire par continent.
def nombreCapitalesContinent(continent) :
    nombre = {}
    for(cap, cont) in continent.items() :
        if cont not in nombre :
            nombre[cont] = 1
        else :
            nombre[cont] +=1
    return nombre

print(nombreCapitalesContinent(continent))

#construire un nouveau dictionnaire avec par continent le
# nombre de capitales et la somme totale des habitants
# comment définirez vous votre dictionnaire ?
# {continent : [nombreCap, totalHab]}

def nombreCapitalesHabitantsContinent(capitales, continent) :
    dicoCapHab = {}
    for (cap, cont) in continent.items() :
        if cont not in dicoCapHab :
            dicoCapHab[cont] = [1, capitales[cap]]
        else :
            dicoCapHab[cont][0] += 1
            dicoCapHab[cont][1] += capitales[cap]
    return dicoCapHab

print(nombreCapitalesHabitantsContinent(capitales, continent))
