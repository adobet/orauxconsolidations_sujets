import random
def newPlateau(l, c) :
    plateau = {}
    for i in range(l) :
        for j in range(c) :
            if random.randint(1, 100) < 50 :
                plateau[(i, j)] = '*'
            else : 
                plateau[(i, j)] = ' '
    return plateau

def getNbLignes(plateau) : 
    n = 0
    for(i, j) in plateau : 
        if i > n : 
            n = i 
    return n + 1

def getNbColonnes(plateau) : 
    n = 0
    for (i, j) in plateau : 
        if j > n : 
            n = j
    return n + 1 

def getVal(plateau, i, j) : 
    return plateau[(i,j)] 

def setVal(plateau, i, j, v) : 
    plateau[(i,j)] = v


def afficherPlateau(plateau) :
    l = getNbLignes(plateau)
    c = getNbColonnes(plateau)  
    print(' ', '--'*(c+2))
    for i in range(l) : 
        print('| ', end = '')
        for j in range(c) : 
            if j < c - 1 : 
                print(getVal(plateau, i, j),' ', end ='')
            else : 
                print(getVal(plateau, i, j), end ='')
        print('| ', end = '')
        print()
    print(' ', '--'*(c+2))

#le voisin de i et j (avec un torre)
def voisinD(i, j, l, c) :
    k = j + 1
    if k == c : 
        k = 0
    return (i, k)

def voisinG(i, j, l, c) :
    k = j - 1
    if k == -1 : 
        k = c - 1
    return (i, k)

def voisinH(i, j, l, c) :
    k = i - 1
    if k == -1 : 
        k = l - 1
    return (k, j)

def voisinB(i, j, l, c) :
    k = i + 1
    if k == l : 
        k = 0
    return (k, j)

def voisin(i, j, l, c, k) : 
    if k == 0 : 
        return voisinD(i, j, l, c) 
    else : 
        if k == 1 : 
            return voisinG(i, j, l, c)
        else : 
            if k == 2 : 
                return voisinH(i, j, l, c)
            else : 
                return voisinB(i, j, l, c)

#si parmi les voisins (stricts) on a au moins 4 noirs -> deviendra noir      
def changePlateauNoir(plateau, i, j) :
    noir = False
    cpt = 0 
    l = getNbLignes(plateau)
    c = getNbColonnes(plateau)  
    for k in range(4) :
        (lc, cc) = voisin(i, j, l, c, k)
        if getVal(plateau, lc, cc) == '*' : 
            cpt += 1
    if cpt>= 4 : 
        noir = True
    return noir

# si parmi les deux voisisn de H et B il y a 2 blancs -> deviendra blanc
def changePlateauBlanc(plateau, i, j) :
    blanc = False
    cpt = 0 
    l = getNbLignes(plateau)
    c = getNbColonnes(plateau)  
    for k in range(2, 4) :
        (lc, cc) = voisin(i, j, l, c, k)
        if getVal(plateau, lc, cc) == ' ' : 
            cpt += 1
    if cpt>= 2 : 
        blanc = True
    return blanc

#un tour de chhangement en utilisant équitablement soit la fonction 
# changePlateauNoir soit changePlateauBlanc
def change(plateau) :
    l = getNbLignes(plateau)
    c = getNbColonnes(plateau)   
    for i in range(l) : 
        for j in range(c) : 
            if random.randint(1, 100) < 50 :
                if changePlateauNoir(plateau, i, j) : 
                    setVal(plateau, i, j, '*')
            else :
                if changePlateauBlanc(plateau, i, j) :
                    setVal(plateau, i, j, ' ')


#jouer en créant un plateau l c , 
# en l'affichant et en faisant k tours de changement

def plusieurs(l, c, k) : 
    pl = newPlateau(l, c)
    afficherPlateau(pl)
    for i in range(k) : 
        change(pl)
    afficherPlateau(pl)

plusieurs(5, 5, 5)