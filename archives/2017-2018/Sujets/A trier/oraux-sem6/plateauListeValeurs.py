import random
def newPlateau(l, c) :
    plateau = [] 
    for i in range(l) :
        for j in range(c) :
            if random.randint(1, 100) < 50 :
                plateau.append('*')
            else : 
                plateau.append(' ')
    return (l, c, plateau) 

def getNbLignes(plateau) : 
    return plateau[0]  

def getNbColonnes(plateau) : 
    return plateau[1] 

def getVal(plateau, i, j) : 
    return plateau[2][i*plateau[1] + j]

def setVal(plateau, i, j, v) : 
    plateau[2][i*plateau[1] + j] = v


def afficherPlateau(plateau) :
    l = getNbLignes(plateau)
    c = getNbColonnes(plateau)  
    print(' ', '--'*(c+2))
    for i in range(l) : 
        print('| ', end = '')
        for j in range(c) : 
            if j < c - 1 : 
                print(getVal(plateau, i, j),' ', end ='')
            else : 
                print(getVal(plateau, i, j), end ='')
        print('| ', end = '')
        print()
    print(' ', '--'*(c+2))

#le voisin de i et j (avec un torre)
def voisinD(i, j, l, c) :
    k = j + 1
    if k == c : 
        k = 0
    return (i, k)

def voisinG(i, j, l, c) :
    k = j - 1
    if k == -1 : 
        k = c - 1
    return (i, k)

def voisinH(i, j, l, c) :
    k = i - 1
    if k == -1 : 
        k = l - 1
    return (k, j)

def voisinB(i, j, l, c) :
    k = i + 1
    if k == l : 
        k = 0
    return (k, j)

def voisin(i, j, l, c, k) : 
    if k == 0 : 
        return voisinD(i, j, l, c) 
    else : 
        if k == 1 : 
            return voisinG(i, j, l, c)
        else : 
            if k == 2 : 
                return voisinH(i, j, l, c)
            else : 
                return voisinB(i, j, l, c)

#si parmi les voisins (stricts) on a au moins 3 blancs -> deviendra blanc      
def changePlateauBlanc(plateau, i, j) :
    blanc = False
    cpt = 0 
    l = getNbLignes(plateau)
    c = getNbColonnes(plateau)  
    for k in range(4) :
        (lc, cc) = voisin(i, j, l, c, k)
        if getVal(plateau, lc, cc) == '*' : 
            cpt += 1
    if cpt>= 3 : 
        blanc = True
    return blanc

# si parmi les deux voisisn de D et G il y a 2 noirs -> deviendra noir
def changePlateauNoir(plateau, i, j) :
    noir = False
    cpt = 0 
    l = getNbLignes(plateau)
    c = getNbColonnes(plateau)  
    for k in range(2) :
        (lc, cc) = voisin(i, j, l, c, k)
        if getVal(plateau, lc, cc) == ' ' : 
            cpt += 1
    if cpt>= 2 : 
        noir = True
    return noir

#un tour de chhangement en utilisant équitablement soit la fonction 
# changePlateauNoir soit changePlateauBlanc
def change(plateau) :
    l = getNbLignes(plateau)
    c = getNbColonnes(plateau)   
    for i in range(l) : 
        for j in range(c) : 
            if random.randint(1, 100) < 50 :
                if changePlateauNoir(plateau, i, j) : 
                    setVal(plateau, i, j, '*')
            else :
                if changePlateauBlanc(plateau, i, j) :
                    setVal(plateau, i, j, ' ')


#jouer en créant un plateau l c , 
# en l'affichant et en faisant k tours de changement

def plusieurs(l, c, k) : 
    pl = newPlateau(l, c)
    afficherPlateau(pl)
    for i in range(k) : 
        change(pl)
    afficherPlateau(pl)

plusieurs(5, 5, 5)