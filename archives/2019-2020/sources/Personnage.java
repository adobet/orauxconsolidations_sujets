public class Personnage{
    private String nom;
    private int tailleOreilles;
    private int tailleBarbe;
    public Personnage(String nom, int barbe, int oreilles){
        this.nom = nom;
        this.tailleOreilles = oreilles;
        this.tailleBarbe = barbe;
    }
    public String getNom(){
        return this.nom;    
    }    
    public void setTailleBarbe(int nouvelleTailleBarbe){
        this.tailleBarbe = nouvelleTailleBarbe;
    }
}
