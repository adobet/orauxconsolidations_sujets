class Truc{
  public static void main(String [] args){
    Personnage gimli = new Personnage("Gimli",65,15);
    String n = gimli.getNom();
    System.out.println(n);
    
    Vaisseau faucon = new Vaisseau("Faucon Millenium", 6, 4);
    int p = faucon.getPuissance();
    System.out.println(p);
    
    Pokemon cocon = new Pokemon("Coconfort", 10.5, 1);
    int a = cocon.getAttaque();
    System.out.println(a);
  }
}

// ====================================================================
// SUJET n° 1
// ====================================================================

class Vaisseau{
    private String nom;
    private int nombrePassagers;
    private int puissanceDeFeu;
    private String proprietaire;
    
    public Vaisseau(String nom, int passagers, int puissance, String proprietaire){
        this.nom = nom;
        this.nombrePassagers = passagers;
        this.puissanceDeFeu = puissance;
        this.proprietaire = proprietaire;
    }
    public int getPuissance(){
        return this.puissanceDeFeu;    
    }    
    public void setNom(String nouveauNom){
        this.nom = nouveauNom;
    }
    // A FAIRE
    public Vaisseau(String nom, int passagers, int puissance){
        this.nom = nom;
        this.nombrePassagers = passagers;
        this.puissanceDeFeu = puissance;
        this.proprietaire = null;
    }
    public String getNom(){ return null;}
    public int nbPassagers(){ return this.nombrePassagers;}
    public void setProprietaire(String nouveauproprietaire){ this.proprietaire = nouveauproprietaire; }
    public String getProprietaire(){ return this.proprietaire; }

}


class ExecutableVaisseau{
  public static void main(String [] args){
    // Faucon Millenium est un Vaisseau qui peut transporter
    // 6 passagers et dont la puissance de feu est de 4
    // Son propriétaire est Lando
    Vaisseau faucon = new Vaisseau("Faucon Millenium", 6, 4, "Lando");
    String nom = faucon.getNom();
    assert "Faucon Millenium".equals(nom);
    assert 6 == faucon.nbPassagers();
    int n = faucon.getPuissance();
    assert n == 4;
    // Le faucon millenium change de proprietaire :
    faucon.setProprietaire("Han");
    assert "Han".equals(faucon.getProprietaire());
    Vaisseau tie = new Vaisseau("Chasseur Tie", 0, 15);
    Vaisseau executor = new Vaisseau("Super Star Destroyer", 38000, 250, "Empire");
    Vaisseau corvette = new Vaisseau("Corvette", 80, 2);
    Flotte empire = new Flotte();
    empire.ajoute(tie);
    empire.ajoute(executor);
    assert "Nouvelle flotte".equals(empire.getNom());
    Flotte alliance = new Flotte("Alliance rebelle");
    alliance.ajoute(faucon);
    alliance.ajoute(tie);
    assert "Alliance rebelle".equals(alliance.getNom());
    System.out.println(alliance.getNom()); // Alliance rebelle
  }
}

class Flotte{
    public Flotte(){}
    public Flotte(String nom){}
    public String getNom(){ return null;}
    public void ajoute(Vaisseau vaisseau){}
}



class Pokemon{
    private String nom;
    private double poids;
    private int attaque;
    public Pokemon(String nomPokemon, double poids, int valeurAttaque){
        this.nom = nomPokemon;
        this.poids = poids;
        this.attaque = valeurAttaque;
    }
    public int getAttaque(){
        return this.attaque;    
    }    
    public void setNom(String nouveauNom){
        this.nom = nouveauNom;
    }
    
    // A FAIRE
    public String getNom(){return this.nom; }
    public double getPoids(){return this.poids; }
    public void setAttaque(int nouvelleAttaque){this.attaque = nouvelleAttaque; }
    public void setPoids(double nouveauPoids){this.poids = nouveauPoids; }
}

class ExecutablePokemon{
  public static void main(String [] args){
    // Coconfort est un pokemon de 10.5kg et sa valeur d'attaque est 1
    Pokemon cocon = new Pokemon("Coconfort", 10.5, 1);
    String nom = cocon.getNom();
    assert "Coconfort".equals(nom);
    assert 1 == cocon.getAttaque();
    double p = cocon.getPoids();
    System.out.println(p);
    // Coconfort change de valeur d'attaque, qui passe à 2
    cocon.setAttaque(2);
    assert 2 == cocon.getAttaque();
    Pokemon dardagnan = new Pokemon("Dardagnan", 29.5, 5);
    Pokemon dracaudeu = new Pokemon("Dracaufeu", 90.5, 4);
    Pokemon bulbizarre = new Pokemon("Bulbizarre", 6.9, 3);
    Pokedex pokedex = new Pokedex();
    pokedex.ajoute(dardagnan);
    pokedex.ajoute(dracaudeu);
    assert "NouveauPokedex".equals(pokedex.getNom());       
    Pokedex pokedexMathieu = new Pokedex("Mathieu");
    pokedexMathieu.ajoute(cocon);
    pokedexMathieu.ajoute(dardagnan);
    pokedexMathieu.ajoute(bulbizarre);
    assert "Mathieu".equals(pokedexMathieu.getNom());
    System.out.println(pokedexMathieu.getNom());
    }
}

class Pokedex{
    public Pokedex(){}
    public Pokedex(String nom){}
    public String getNom(){ return null;}
    public void ajoute(Pokemon poke){}
}

class Personnage{
    private String nom;
    private int tailleOreilles;
    private int tailleBarbe;
    public Personnage(String nom, int barbe, int oreilles){
        this.nom = nom;
        this.tailleOreilles = oreilles;
        this.tailleBarbe = barbe;
    }
    public String getNom(){
        return this.nom;    
    }    
    public void setTailleBarbe(int nouvelleTailleBarbe){
        this.tailleBarbe = nouvelleTailleBarbe;
    }
    
    //  FAIRE
    public int getBarbe(){return this.tailleBarbe; }
    public int getOreilles(){return this.tailleOreilles; }
    public void setNom(String nouveauNom){this.nom = nouveauNom; }
    public void setOreilles(int nouvellesOreilles){this.tailleOreilles = nouvellesOreilles; }
}

class ExecutablePersonnage{
  public static void main(String [] args){
    // Gimli a une barbe de 65cm et ses oreilles mesurent 15cm
    Personnage gimli = new Personnage("Gimli", 65, 15);
    String nom = gimli.getNom();
    assert "Gimli".equals(nom);
    assert 65 == gimli.getBarbe();
    int oreilles = gimli.getOreilles();
    assert 15 == oreilles;
    // La barbe de Gimli a poussé : elle mesure maintenant 18 cm
    gimli.setTailleBarbe(18);
    assert 18 == gimli.getBarbe();
    Personnage legolas = new Personnage("Legolas", 0, 35);
    Personnage frodo = new Personnage("Frodo", 1, 25);
    Personnage grandPas = new Personnage("GrandPas", 20, 8);
    Groupe gpe = new Groupe();
    gpe.ajoute(gimli);
    gpe.ajoute(legolas);
    assert "Nouveau Groupe".equals(gpe.getNom());     
    Groupe fraternite = new Groupe("La Fraternité");
    fraternite.ajoute(gimli);
    fraternite.ajoute(frodo);
    fraternite.ajoute(grandPas);
    assert "La Fraternité".equals(fraternite.getNom());     
    System.out.println(fraternite.getNom());
  }
}

class Groupe{
    public Groupe(){}
    public Groupe(String nom){}
    public String getNom(){return null;}
    public void ajoute(Personnage perso){}
}
