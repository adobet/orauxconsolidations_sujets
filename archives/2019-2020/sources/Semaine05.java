import java.util.ArrayList;

class Evaluation{
    private String matiere;
    private int note;
    public Evaluation(String m, int n){
        this.matiere = m;
        this.note = n;
    }
    public String getMatiere(){ return this.matiere; }
    public int getNote(){ return this.note; }
    public void setNote(int nouvelleNote){this.note=nouvelleNote; }
}

class LesNotes {
    private ArrayList<Evaluation> lesEval;
    private String monNom;
    public LesNotes(String nom){
        this.monNom = nom;
        this.lesEval = new ArrayList<>();
    }
    public void ajouteEvaluation(String matiere, int note){
        Evaluation eval = new Evaluation(matiere, note);
        this.lesEval.add(eval);
    }
    public int mystere(){
        int nb = 0;
        for (Evaluation eval : this.lesEval){
            if (eval.getNote() > 15){
                nb+=1;
            }
        }
        return nb;
    }
}

class Executable{
    public static void main(String [] args){
        LesNotes notesBob = new LesNotes("Bob");
        notesBob.ajouteEvaluation("POO", 15);
        notesBob.ajouteEvaluation("BD", 17);
        LesNotes notesCalo = new LesNotes("Carlo");
        notesCalo.ajouteEvaluation("POO", 19);
        System.out.println(notesCalo.mystere());
    }
}
