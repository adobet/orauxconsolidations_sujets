public class Vaisseau{
    private String nom;
    private int nombrePassagers;
    private int puissanceDeFeu;
    private String proprietaire;
    
    public Vaisseau(String nom, int passagers, int puissance, String proprietaire){
        this.nom = nom;
        this.nombrePassagers = passagers;
        this.puissanceDeFeu = puissance;
        this.proprietaire = proprietaire;
    }
    public int getPuissance(){
        return this.puissanceDeFeu;    
    }    
    public void setNom(String nouveauNom){
        this.nom = nouveauNom;
    }
}
