public class Executable{
    public static void main(String [] args){
        Personnage batman = new Personnage("Bruce Wayne");
        Personnage joker = new Personnage("The joker");
        Personnage bruce =  new Personnage("Bruce Wayne");
    
        Personnage mechant = joker;


        System.out.println(batman == bruce); // false - false
        System.out.println(mechant == joker); // true - true   
        
        System.out.println(batman.equals(bruce)); // false - true
        System.out.println(batman.equals(batman)); // true - true
        System.out.println(joker.equals(mechant)); // true - true
        
        System.out.println(joker.equals(null)); // false - false
        
        Truc machin = new Truc();
        System.out.println(joker.equals(machin)); // false
        
        System.out.println(batman); // Personnage@15d4514554
        

        // System.out.println(batman.equals(joker)); // affichage 3
        
        // System.out.println(mechant == joker); // affichage 4 
        
        // Truc machin = new Truc();
        // System.out.println(machin);
        
        // System.out.println(Truc.max(15,6)); //15
        // System.out.println(machin.max(15,6)); //15 (aucun interet)
        }
}

// le == compare les instances
// a == b -> vrai si a et b pointent vers la même instance dans le tas

// la méthode equals() de base (si on ne l'override pas)
// elle se comporte comme le ==

// Mais souvent, on l'override


// une méthode static :
//    - c'est une méthode dans laquelle on ne peut par utiliser this
//    - c'est une méthode qui peut être utiliser sans créer d'intance de la classe
// Par exemple une fonction qui renvoie le max de deux nombres passes en parametres
