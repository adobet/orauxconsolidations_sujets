public class Personnage{
    private String nom;
    private String prenom;
    private double age;

    
    public Personnage(String nom){ 
        this.nom = nom;
        this.prenom = "Toto";
        this.age = 22.0;
    }
    
    @Override
    public boolean equals(Object obj){
        if (obj == null){ 
            return false;
        }
        if (obj instanceof Personnage){
            Personnage autre = (Personnage) obj; 
            if (this.nom.equals(autre.nom) && this.prenom.equals(autre.prenom)){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
    
    @Override
    public int hashCode(){
        Double a = this.age;
        return this.nom.hashCode() + this.prenom.hashCode() + a.hashCode();
    }
}


// transitif : si x=y et y=z alors x=z

// si deux objets sont 'equals' alors ils doivent avoir le même hashCode
// en revanche, la réciproque n'est pas nécessaire

// Tous les objets ont une méthode equals() et tous les objets ont une méthode hashCode()
